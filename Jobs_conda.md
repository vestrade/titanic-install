
# Notes on cluster usage.

The /home is shared between master and nodes and between all users; the available space is about 100GB. For large amounts of data (>500Mo), one can put it on one node in the shared /data/ folder (ex: /data/titanic-1/) and any of the nodes will be able to read it (but not write on it): ask administrators for a personal space on /data. 

In order to use the cluster ; need to create a slurm account, under the 'tau' main account/team. Check the slurm documentation on how to do that. Use the commands 'sinfo' and 'squeue' to monitor your jobs, and add them to the queue using 'sbatch'. To use GPUs, you must specify, when submitting a job, the option '--gres=gpu:i' with 'i' the number of GPU you want to use, and you could know which GPU is affected to your job using the $CUDA\_AVAILABLE\_DEVICES variable (set by Slurm) in your bash script. You might want to read the quick start guide on slurm before trying it out : https://slurm.schedmd.com/quickstart.html


An example of command to run your jobs would be : 
> sbatch --gres=gpu:4 --time=24-00:00:00 -N1 --no-kill --error=slurm-err-%j.out --output=slurm-out-%j.out my_script.sh

or for a lighter job : 
> sbatch --gres=gpu:1 --time=24-00:00:00 -c 6 --no-kill --error=slurm-err-%j.out --output=slurm-out-%j.out my_script.sh

Here, the used options are:
- _--gres=gpu:4_ to allocate 4 GPUs for the job
- _--time=24-00:00:00_ to set the time limit to 24 days, max is 28 days. Without any specification of time, the default is set to 1 hour.
- _-N1_ is to allocate 1 full node to the job.  **WARNING**: using _-N1_ reserves the full node, so don't use this option if you don't plan to use the 4 GPUs of the server. Prefer using _-c X_ with _X_ your required number of CPUs. 
- _--no-kill_ is an option for slurm to not kill the job for a random reason (Need more details on this)
- _--error=slurm-err-%j.out --output=slurm-o-%j.out_ is to separate both output script and error script (stdout and stderr), as per default they are fused into one file (which is annoying with tensorflow's hundreds of warnings)
- _my\_script.sh_, finally the script to run. Beware, the script name/path has to be put at the **end of the command**, else slurm will ignore the options **after the script name**. Moreover, the script must have open permissions for slurm to run it. To avoid any ambiguous situations, do a _chmod 775 my\_script.sh_

For an interactive job, use **srun** instead of **sbatch**

_ToDo: Notes on jupyter-notebook_ 


# Integration with conda. 
In order to successfully use personal libraries on the cluster, one can install miniconda(or a variant) on their home dir (add to the $PATH the conda executable(answer 'yes' to the last question.) ; you might have to rexecute the .bashrc to re-init the $PATH; just disconnect and reconnect to the server for an easy way) and add their extra libraries.
To do that, initialize a "clean" conda environment using : 
> conda create --name example_name python=python_version.

The Python version MUST be set to either 2.7 or 3.5 as the GPU libraries are only installed with these.
Then, activate the new environment using:
> source activate example_name

to successfully use the various ML libraries installed, you must install a version of **numpy and pandas** on your environment. So install your personal extra libraries along with numpy on the list - USE pip ! Conda seems to be a bit messy with numpy.
ex: we also install rpy2 for example.

> pip install numpy pandas rpy2


## Using the preinstalled libraries 
As the home only allows limited memory, the heavy ML libraries have been installed on the various nodes. 

One bad way to link the pre-installed libraries to your conda environment,you could modify the PYTHONPATH in the .bashrc :
IF you use python 2.7 :
> export PYTHONPATH='/usr/local/lib/python2.7/dist-packages:$PYTHONPATH'
IF you use python 3.5 :
> export PYTHONPATH='/usr/local/lib/python3.5/dist-packages:$PYTHONPATH'

/!\ Careful if you use both Python2 and 3 : you must reset the $PYTHONPATH variable each time you change.

## A better way to do it

A more intelligent way of doing it without modifying savagely the $PYTHONPATH would be to ask conda to set those variables each time you activate the environment and unset them if you deactivate. That way, everything is working inside and outside the conda environment. More details in here : https://conda.io/docs/using/envs.html#saved-environment-variables . Use the **adapt_conda.sh** script to do it automatically : it goes through your conda environments to set activate and deactivate scripts.

To adapt your conda environment to the titanic cluster, you can run the _adapt\_conda.sh_ script on the cluster. It will modify all conda environements' parameters, so that way: 
* The conda libraries of your environement will be prioritized over the local librairies installed on the nodes.
* The local libraries used have GPU acceleration (and CPU optimizations for tensorflow)
* The $PYTHONPATH will be modified _only_ in your various conda environments : as soon as you quit the conda environment it will go back to the original $PYTHONPATH.


# Installed libraries: 

For Torch7(lua) users:
Torch has been installed in /usr/local/torch on all nodes with CuDnn5.1
use the /usr/local/torch/install/bin/th executable in your scripts to run it.

The pre-installed libraries in python 2.7 and 3.5 are :
- scipy(0.19.1)
- scikit-learn(0.19.0)
- joblib(0.11)
- matplotlib (2.0.2)
- pytorch\[Cuda8.0\](0.2.0)
- keras (2.0.8)
- tensorflow\[MKL, Broadwell optimizations, CuDnn 6.0.21, Cuda8.0, Jmalloc\](1.2.1)

Lua Libraries :
- torch7 [CuDnn 5.1]
