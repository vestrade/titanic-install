#!/bin/bash

if [ -d "${HOME}/miniconda3" ]; then
    installdir="${HOME}/miniconda3"
else
    if [ -d "${HOME}/miniconda2" ]; then
	installdir="${HOME}/miniconda2"
    else
	cur='pwd'
	echo -ne "Specify the conda install path :\n"
	read filepath
	
	if [ ! -d $filepath ]; then
	    echo "Please insert a correct path"
	    sleep 1
	    exit 1
	else
	    installdir="${filepath}"
	fi
    fi
    
fi

read -p "Confirm conda install in ${installdir}, continue? [y/n] :" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    for envs in "${installdir}"/envs/*
    do
	mkdir -p "${envs}/etc/conda/activate.d/"
	mkdir -p "${envs}/etc/conda/deactivate.d/"
	if [ ! -f "${envs}/etc/conda/activate.d/env_vars.sh" ]; then
	    touch "${envs}/etc/conda/activate.d/env_vars.sh"
	fi
	if [ ! -f "${envs}/etc/conda/deactivate.d/env_vars.sh" ]; then
	    touch "${envs}/etc/conda/deactivate.d/env_vars.sh"
	fi
	# ToDo: Detect Python Version : Script
	PYV=`${envs}/bin/python -c "import sys;t='{v[0]}.{v[1]}'.format(v=list(sys.version_info[:2]));sys.stdout.write(t)";`
	bashinit='#!/bin/sh'
	activate='export PYTHONPATH="'${envs}'/lib/python'${PYV}'/dist-packages:'${envs}'/lib/python'${PYV}'/site-packages:/usr/local/lib/python'${PYV}'/dist-packages:$PYTHONPATH"'
	deactivate='export PYTHONPATH="${PYTHONPATH/"'${envs}'/lib/python'${PYV}'/dist-packages:'${envs}'/lib/python'${PYV}'/site-packages:/usr/local/lib/python'${PYV}'/dist-packages:"/""}"'

	if ! grep -qs $bashinit "${envs}/etc/conda/activate.d/env_vars.sh"; then
	    echo $bashinit >> "${envs}/etc/conda/activate.d/env_vars.sh"
	fi
	if ! grep -qs $bashinit "${envs}/etc/conda/deactivate.d/env_vars.sh"; then
	    echo $bashinit >> "${envs}/etc/conda/deactivate.d/env_vars.sh"
	fi

	if ! grep -qs $activate "${envs}/etc/conda/activate.d/env_vars.sh"; then
	    echo $activate >> "${envs}/etc/conda/activate.d/env_vars.sh"
	fi

	if ! grep -qs $activate "${envs}/etc/conda/deactivate.d/env_vars.sh"; then
	    echo $deactivate >> "${envs}/etc/conda/deactivate.d/env_vars.sh"
	fi

	
    done
    
fi

echo " Done !"
