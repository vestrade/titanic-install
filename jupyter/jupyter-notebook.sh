#!/bin/bash

# Run an interactive session launching jupyter notebook

OPTIONS=""
OPTIONS="$OPTIONS --account=tau"
OPTIONS="$OPTIONS --job-name=jupyter-notebook"
# OPTIONS="$OPTIONS -p besteffort"                # best effort
# OPTIONS="$OPTIONS --output=jupyter.log"         # Log output
# OPTIONS="$OPTIONS --nodelist titanic-1"         # list of node
OPTIONS="$OPTIONS --ntasks=1"
OPTIONS="$OPTIONS --cpus-per-task=8"            # number of CPU requested
OPTIONS="$OPTIONS --gres=gpu:1"                 # number of GPU requested
OPTIONS="$OPTIONS -t 09:00:00"                  # max runtime is 9 hours


echo "srun $OPTIONS node_jupyter.sh" 
srun $OPTIONS node_jupyter.sh
