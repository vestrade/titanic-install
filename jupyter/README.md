# Jupyter notebook usage on the cluster

Here are the scripts for running jupyter notebooks on the titanic cluster.

## Requirement 

- miniconda or Anaconda installed
- Jupyter installed
- put the ```adapt_conda.sh``` script in your HOME directory
- put the ```node_jupyter.sh``` script in your HOME directory
- put the ```jupyter-notebook.sh``` script in your HOME directory

## Setup

In the ```node_jupyter.sh``` script :

Change l.14 ```WORKDIR="/home/tao/${USER}/workspace/"``` to your working directory.
Or comment the two lines if you run the script from your working directory.


In the ```run_nb.sh``` script :

Change l.28 ```WORKDIR="/home/tao/${USER}/workspace/"``` to your working directory.
Or comment the two lines if you run the script from your working directory.

## Run an interactive jupyter notebook

### Procedure

Simply run the ```jupyter-notebook.sh``` script.
You may have to press ```enter``` to finish to run the ```adapt_conda.sh``` script

Then follow the printed instruction to open a ssh tunnel to the working node.

Now just open the link in your local browser like an ordinary notebook.

### Options

You can choose the slurm options (number of CPU, max Time, number of GPU) by modifying the ```jupyter-notebook.sh``` script.

## Run non-interactive jupyer notebook

### Procedure

Maybe your notebooks take a long time to run and render.

One solution is to run the notebook in a non-interactive way and save the result.

Use the ```run_nb.sh``` script to run and save a notebook on a node :

```sbatch run_nb.sh Plot_awsome_stuff.ipynb```

Once finished you can run your notebook in an interactive way to see the results.

### Options

You can choose the slurm options (number of CPU, max Time, number of GPU) by modifying the ```run_nb.sh``` special #SBATCH comments.

You can also change the notebook cell's timeout (l.31). 
If some of your notebook cells require more time then increase the value (900 seconds in the current script).
