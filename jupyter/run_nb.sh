#!/usr/bin/sbatch

#SBATCH --account=tau
#SBATCH --job-name=JobName
#SBATCH --output=%j_slurm.log
#SBATCH -t 8:00:00             # max runtime hours:min:sec
#SBATCH --cpus-per-task=9
#SBATCH --gres=gpu:1

#OLDSBATCH --nodelist titanic-3
#OLDSBATCH -N 1

# sbatch ... -C kepler  ... for Titan Blacks (kepler GPU architechture)
# sbatch ... -C pascal ... for GTX1080Ti (pascal GPU architechture)

date
hostname
pwd

# Manually add miniconda to PATH. Don't know why the .basrc is not correctly sourced
# export PATH="/home/tao/${USER}/miniconda3/bin:$PATH"
# source /home/tao/${USER}/miniconda3/bin/activate default

# Activate python and use local libraries
bash "${HOME}/adapt_conda.sh"
source activate py35

WORKDIR="/home/tao/${USER}/workspace/"
cd $WORKDIR

jupyter nbconvert --ExecutePreprocessor.timeout=900 --to notebook --execute $1 --output $1

echo "DONE"
date

exit 0
